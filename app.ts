import * as Drash from "https://deno.land/x/drash@v2.4.0/mod.ts";

// Create your resource

class HomeResource extends Drash.Resource {
  public paths = ["/"];

  public GET(_request: Drash.Request, response: Drash.Response): void {
    return response.json({
      designation: {
        domain: Deno.env.get("SSC_DOMAIN"),
        environment: Deno.env.get("SSC_ENVIRONMENT"),
        label: Deno.env.get("SSC_LABEL"),
      },
      time: new Date(),
    });
  }
}

const server = new Drash.Server({
  hostname: "0.0.0.0",
  port: 8000,
  protocol: "http",
  resources: [HomeResource],
});

server.run();

console.log(`Server running at ${server.address}.`);
